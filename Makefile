# Makefile
INSTALL = install
MKDIR = mkdir
CP = cp


EXES = all

all:

install: install_client install_server

install_client:
	@${INSTALL} -D -m 0755 init.d/access ${DESTDIR}/etc/init.d/access
	@${INSTALL} -D -m 0644 conf.d/access ${DESTDIR}/etc/conf.d/access

install_server:
	@${INSTALL} -D -m 0755 scripts/cl-access-setup ${DESTDIR}/usr/sbin/cl-access-setup
	@${INSTALL} -D -m 0755 scripts/cl-access-add ${DESTDIR}/usr/sbin/cl-access-add
	@${MKDIR} -p ${DESTDIR}/usr/share/calculate
	@${CP} -r access ${DESTDIR}/usr/share/calculate
